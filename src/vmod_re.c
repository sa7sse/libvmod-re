/*-
 * Copyright 2013 - 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#define PCRE2_CODE_UNIT_WIDTH 8

#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vre.h"
#include "vre_pcre2.h"
#include "vsb.h"

#include "vcc_if.h"

#define MAX_MATCHES	11
#define MAX_OV		((MAX_MATCHES) * 2)

struct vmod_re_regex {
	unsigned		magic;
#define VMOD_RE_REGEX_MAGIC 0x955706ee
	vre_t			*vre;
	struct vre_limits	vre_limits;
};

typedef struct ov_s {
	unsigned		magic;
#define OV_MAGIC 0x844bfa39
	const char		*subject;
	int			ovector[MAX_OV];
} ov_t;

static void
errmsg(VRT_CTX, const char *fmt, ...)
{
	va_list args;

	AZ(ctx->method & VCL_MET_TASK_H);
	va_start(args, fmt);
	if (ctx->vsl)
		VSLbv(ctx->vsl, SLT_VCL_Error, fmt, args);
	else
		VSLv(SLT_VCL_Error, 0, fmt, args);
	va_end(args);
}

static vre_t *
re_compile(const char *pattern, unsigned options, char *errbuf,
    size_t errbufsz, int *erroffset)
{
	static vre_t *vre;
	struct vsb vsb[1];
	int errcode;

	vre = VRE_compile(pattern, options, &errcode, erroffset, 1);
	if (vre != NULL)
		return (vre);

	AN(VSB_init(vsb, errbuf, errbufsz));
	AZ(VRE_error(vsb, errcode));
	AZ(VSB_finish(vsb));
	VSB_fini(vsb);
	return (NULL);
}

VCL_VOID
vmod_regex__init(VRT_CTX, struct vmod_re_regex **rep, const char *vcl_name,
		 VCL_STRING pattern, VCL_INT limit, VCL_INT limit_recursion)
{
	struct vmod_re_regex *re;
	vre_t *vre;
	char errbuf[VRE_ERROR_LEN];
	int erroffset;
	const char *error;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(rep);
	AZ(*rep);
	AN(vcl_name);
	AN(pattern);

	if (limit < 1) {
		VRT_fail(ctx, "vmod re: invalid limit %ld in %s constructor",
			 limit, vcl_name);
		return;
	}

	if (limit_recursion < 1) {
		VRT_fail(ctx, "vmod re: invalid limit_recursion %ld "
			 "in %s constructor", limit_recursion, vcl_name);
		return;
	}

	vre = re_compile(pattern, 0, errbuf, sizeof errbuf, &erroffset);
	if (vre == NULL) {
		VRT_fail(ctx, "vmod re: error compiling regex \"%s\" in %s "
			 "constructor: %s (at offset %d)", pattern, vcl_name,
			 errbuf, erroffset);
		return;
	}

	ALLOC_OBJ(re, VMOD_RE_REGEX_MAGIC);
	AN(re);
	re->vre = vre;
	re->vre_limits.match = limit;
	re->vre_limits.depth = limit_recursion;
	*rep = re;
}

VCL_VOID
vmod_regex__fini(struct vmod_re_regex **rep)
{
	struct vmod_re_regex *re;

	if (rep == NULL || *rep == NULL)
		return;
	re = *rep;
	*rep = NULL;
	CHECK_OBJ_NOTNULL(re, VMOD_RE_REGEX_MAGIC);
	if (re->vre != NULL)
		VRE_free(&re->vre);
	FREE_OBJ(re);
}

static VCL_BOOL
match(VRT_CTX, const vre_t *vre, VCL_STRING subject, struct vmod_priv *task,
	const struct vre_limits *vre_limits)
{
	ov_t *ov;
	int i, r = 0, s, nov[MAX_OV];
	size_t cp;
	pcre2_match_context *re_ctx;
	pcre2_match_data *data;
	pcre2_code *re;
	PCRE2_SIZE *ovector;

	AN(vre);
	if (subject == NULL)
		subject = "";

	if (task->priv == NULL) {
		if ((task->priv = WS_Alloc(ctx->ws, sizeof(*ov))) == NULL) {
			VSLb(ctx->vsl, SLT_VCL_Error, "vmod re error: "
			     "insufficient workspace for backref data");
			return 0;
		}
		task->len = -1;
		AZ(task->methods);
		ov = (ov_t *) task->priv;
		ov->magic = OV_MAGIC;
	}
	else {
		AN(WS_Allocated(ctx->ws, task->priv, sizeof(*ov)));
		CAST_OBJ_NOTNULL(ov, task->priv, OV_MAGIC);
	}

	// BEGIN duplication with vre
	re = VRE_unpack(vre);
	AN(re);
	data = pcre2_match_data_create_from_pattern(re, NULL);
	if (data == NULL) {
		VRT_fail(ctx, "vmod_re: failed to create match data");
		return 0;
	}
	// END duplication with vre

	// BEGIN unneeded overhead (unless we get access to re_ctx also)
	re_ctx = pcre2_match_context_create(NULL);
	if (re_ctx == NULL) {
		VRT_fail(ctx, "vmod_re: failed to create context");
		goto out;
	}
	pcre2_set_depth_limit(re_ctx, vre_limits->depth);
	pcre2_set_match_limit(re_ctx, vre_limits->match);
	// END unneeded overhead

	s = pcre2_match(re, (PCRE2_SPTR)subject, PCRE2_ZERO_TERMINATED, 0,
	    0, data, re_ctx);

	if (s <= PCRE2_ERROR_NOMATCH) {
		if (s < PCRE2_ERROR_NOMATCH)
			VSLb(ctx->vsl, SLT_VCL_Error,
			     "vmod re: regex match returned %d", s);
		goto out;
	}
	if (s > MAX_MATCHES) {
		VSLb(ctx->vsl, SLT_VCL_Error,
		     "vmod re: capturing substrings exceed max %d",
		     MAX_MATCHES - 1);
		s = MAX_MATCHES;
	}
	ovector = pcre2_get_ovector_pointer(data);
	assert (s <= pcre2_get_ovector_count(data));

	task->len = sizeof(*ov);
	ov->subject = subject;

	for (i = 0; i < s * 2; i++)
		ov->ovector[i] = ovector[i];
	for ( ; i < MAX_OV; i++)
		ov->ovector[i] = -1;

	r = 1;

out: // XXX goto because this might be throw-away code
	AN(data);
	pcre2_match_data_free(data);
	if (re_ctx != NULL)
		pcre2_match_context_free(re_ctx);
	return (r);
}

static VCL_STRING
backref(VRT_CTX, VCL_INT refnum, VCL_STRING fallback,
    const struct vmod_priv *task)
{
	ov_t *ov;
	const char *substr, *start;
	int len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(fallback);
	AN(task);

	if (refnum < 0 || refnum >= MAX_MATCHES) {
		VSLb(ctx->vsl, SLT_VCL_Error,
		     "vmod re: backref %ld out of range", refnum);
		return fallback;
	}

	if (task->priv == NULL) {
		VSLb(ctx->vsl, SLT_VCL_Error,
		     "vmod re: backref called without prior match");
		return fallback;
	}
	if (task->len <= 0)
		return fallback;

	AN(WS_Allocated(ctx->ws, task->priv, sizeof(*ov)));
	CAST_OBJ_NOTNULL(ov, task->priv, OV_MAGIC);

	refnum *= 2;
	assert(refnum + 1 < MAX_OV);
	if (ov->ovector[refnum] == -1)
		return fallback;

	start = ov->subject + ov->ovector[refnum];
	len = ov->ovector[refnum+1] - ov->ovector[refnum];
	assert(len <= ov->ovector[1] - ov->ovector[0]);
	if (start[len] == '\0')
		substr = start;
	else
		substr = WS_Printf(ctx->ws, "%.*s", len, start);
	if (substr == NULL) {
		VSLb(ctx->vsl, SLT_VCL_Error,
		     "vmod re: insufficient workspace");
		return fallback;
	}
	return substr;
}

static inline const struct vre_limits *
get_limits(const struct vmod_re_regex *re, struct vre_limits *limits,
	   VCL_INT limit, VCL_INT limit_recursion)
{
	if (limit <= 0 && limit_recursion <= 0)
		return (&re->vre_limits);

	if (limit > 0)
		limits->match = limit;
	else
		limits->match = re->vre_limits.match;

	if (limit_recursion > 0)
		limits->depth = limit_recursion;
	else
		limits->depth = re->vre_limits.depth;

	return (limits);
}

VCL_BOOL
vmod_regex_match(VRT_CTX, struct vmod_re_regex *re, VCL_STRING subject,
		 VCL_INT limit, VCL_INT limit_recursion)
{
	struct vmod_priv *task;
	struct vre_limits buf;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(re, VMOD_RE_REGEX_MAGIC);
	AN(re->vre);

	task = VRT_priv_task(ctx, re);

	if (task == NULL) {
		errmsg(ctx, "vmod re: no priv - out of workspace?");
		return (0);
	}

	AN(task);
	task->len = 0;

	return match(ctx, re->vre, subject, task,
		     get_limits(re, &buf, limit, limit_recursion));
}

VCL_STRING
vmod_regex_backref(VRT_CTX, struct vmod_re_regex *re, VCL_INT refnum,
		   VCL_STRING fallback)
{
	struct vmod_priv *task;

	CHECK_OBJ_NOTNULL(re, VMOD_RE_REGEX_MAGIC);

	task = VRT_priv_task(ctx, re);
	if (task == NULL) {
		errmsg(ctx, "vmod re: no priv - out of workspace?");
		return (0);
	}
	return backref(ctx, refnum, fallback, task);
}

VCL_BOOL
vmod_match_dyn(VRT_CTX, struct vmod_priv *task, VCL_STRING pattern,
	       VCL_STRING subject, VCL_INT limit, VCL_INT limit_recursion)
{
	vre_t *vre;
	char errbuf[VRE_ERROR_LEN];
	int erroffset;
	VCL_BOOL dyn_return;
	struct vre_limits limits;

	AN(pattern);
	AN(task);

	if (limit < 1) {
		errmsg(ctx, "vmod re: invalid limit %d for regex \"%s\"",
		       limit, pattern);
		return 0;
	}

	if (limit_recursion < 1) {
		errmsg(ctx, "vmod re: invalid limit_recursion %d "
		       "for regex \"%s\"", limit_recursion, pattern);
		return 0;
	}

	limits.match = limit;
	limits.depth = limit_recursion;

	task->len = 0;
	vre = re_compile(pattern, 0, errbuf, sizeof errbuf, &erroffset);
	if (vre == NULL) {
		VSLb(ctx->vsl, SLT_VCL_Error,
		     "vmod re: error compiling regex \"%s\": %s (position %d)",
		     pattern, errbuf, erroffset);
		return 0;
	}

	dyn_return = match(ctx, vre, subject, task, &limits);

	VRE_free(&vre);
	return dyn_return;
}

VCL_STRING
vmod_backref_dyn(VRT_CTX, struct vmod_priv *task, VCL_INT refnum,
		 VCL_STRING fallback)
{
	return backref(ctx, refnum, fallback, task);
}

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{

	(void) ctx;
	return VERSION;
}
